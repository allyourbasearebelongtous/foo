require 'rspec'
require_relative '../src/array_merger'

RSpec.describe ArrayMerger do
  describe "#remove_duplicates" do
    it "removes duplicate elements from the merged array" do
      array1 = [1, 2, 3]
      array2 = [3, 4, 5]
      array3 = [5, 6, 7]
      merger = ArrayMerger.new(array1, array2, array3)
      merger.merge_arrays
      merger.remove_duplicates
      
      expect(merger.instance_variable_get(:@merged_array)).to eq([1, 2, 3, 4, 5, 6, 7])
    end
  end
end