class ArrayMerger
  def initialize(array1, array2, array3)
    @array1 = array1
    @array2 = array2
    @array3 = array3
  end

  def merge_arrays
    @merged_array = @array1 + @array2 + @array3
  end

  def remove_duplicates
    @merged_array = @merged_array.uniq
  end

  def display_merged_array
    puts "Merged Array: #{@merged_array}"
  end
end

if __FILE__ == $0
    # Example usage:
    array1 = [1, 2, 3]
    array2 = [4, 5, 6]
    array3 = [7, 8, 9]

    merger = ArrayMerger.new(array1, array2, array3)
    merger.merge_arrays
    merger.remove_duplicates
    merger.display_merged_array
end